<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
<meta charset="<?php bloginfo('charset'); ?>">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="google-site-verification" content="8UeiofC42IIbyMF26po9sm_teSV9oqaUuuZbLsW21z4" />
<title><?php bloginfo('name'); { echo ' | '; } if(wp_title('', false)) {} else { echo bloginfo('description'); }wp_title(''); ?></title>

<meta name="description" content="Codehints, Anish M Alias, HTML, HTML5, CSS, CSS3, Jquery, Wordpress, Android, SASS, LESS, Angular">
<meta name="viewport" content="width=device-width, initial-scale=1">

<?php wp_head(); ?>

<link rel="shortcut icon" href="<?php echo get_bloginfo('template_directory'); ?>/img/fav-icon.png" />
<link rel="stylesheet" href="<?php echo get_bloginfo('template_directory'); ?>/css/main.css">
<link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700,900" rel="stylesheet">
<script src="<?php echo get_bloginfo('template_directory'); ?>/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-67055077-1', 'auto');
  ga('send', 'pageview');

</script>


</head>
	<body>
		<header class="header ch-l-header__new">
        <?php get_template_part( 'dummy', get_post_format() ); ?>
        <div class="container">
            <div class="row">
                <div class="col-sm-3 col-xs-8">
                    <h1 class="ch-l-logo__new">
                          <a href="<?php echo home_url();?>">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                            width="837.706px" height="485.153px" viewBox="0 0 837.706 485.153" enable-background="new 0 0 837.706 485.153"
                            xml:space="preserve">
                            <g>
                            <path fill="#5D6F78" d="M5.058,68.789c0-43.112,27.366-65.936,82.103-68.472v41.21C71.731,43.429,64.02,51.78,64.02,66.57v130.604
                            c0,13.948,7.712,21.982,23.141,24.092v40.576c-54.737-2.115-82.103-26.732-82.103-73.861V68.789z M98.889,0
                            c56.847,0.426,85.273,23.671,85.273,69.74v23.458h-60.23V66.57c0-16.058-8.351-24.513-25.043-25.36V0z M162.843,242.822
                            c-14.212,12.046-35.531,18.386-63.954,19.02v-40.576c16.692-1.476,25.043-9.51,25.043-24.092 M123.933,197.174v-34.553h60.23
                            v26.945c0,23.458-7.107,41.21-21.319,53.256"/>
                            <path class="ch-l-logo-ach__left" fill="#4699D4" d="M287.175,220.134c0,8.181,9.756,12.271,29.274,12.271v27.17h-31.972c-18.625,0-31.331-4.294-38.124-12.884
                            s-10.189-21.474-10.189-38.652l0.526-19.458c0.465-8.647,0.701-15.132,0.701-19.458c0-14.374-8.124-21.561-24.366-21.561v-31.903
                            c17.179,0,25.768-10.282,25.768-30.852c0-3.971-0.293-9.874-0.876-17.705c-0.468-7.828-0.701-13.73-0.701-17.705
                            c0-30.501,15.776-45.751,47.329-45.751h31.903v27.346h-5.114c-16.108,0-24.16,4.032-24.16,12.095v59.95
                            c0,13.322-8.094,22.788-24.281,28.397c16.187,5.609,24.281,15.193,24.281,28.748V220.134"/>
                            <path class="ch-l-logo-ach__right" fill="#59B35D" d="M441.77,147.563c-17.063,0-25.593,10.228-25.593,30.676c0,3.975,0.291,9.877,0.876,17.705
                            c0.583,7.831,0.876,13.733,0.876,17.705c0,30.619-15.836,45.927-47.504,45.927h-32.079v-27.17h4.032
                            c16.71,0,25.067-4.089,25.067-12.271v-59.95c0-13.555,8.179-23.139,24.541-28.748c-16.362-4.908-24.541-14.374-24.541-28.397
                            v-59.95c0-8.063-9.701-12.095-29.099-12.095V3.647h32.079c31.553,0,47.329,15.311,47.329,45.927c0,3.975-0.293,9.877-0.876,17.705
                            c-0.468,7.713-0.701,13.558-0.701,17.529c0,20.57,8.529,30.852,25.593,30.852V147.563z"/>
                            <path fill="#5D6F78" d="M533.388,5.706V256.77h-57.377V5.706H533.388z M542.264,5.706h29.798c53.677,0,80.518,23.567,80.518,70.691
                            v108.731c0,47.763-27.054,71.642-81.152,71.642h-29.164v-42.795h20.605c10.778,0,18.545-1.793,23.3-5.389
                            c4.755-3.591,7.132-9.193,7.132-16.801V74.178c0-9.084-2.486-15.637-7.449-19.654c-4.968-4.012-12.839-6.023-23.617-6.023h-19.971
                            V5.706z"/>
                            <path fill="#5D6F78" d="M753.384,5.706V256.77h-58.011V5.706H753.384z M762.26,5.706h75.446v42.161H762.26V5.706z M762.26,105.244
                            h71.009v41.844H762.26V105.244z M837.706,256.77H762.26v-43.429h75.446V256.77z"/>
                            <path fill="#5D6F78" d="M22.307,482.78V308.955h32.155v71.073h78.548v-71.073h32.155V482.78H133.01v-77.361H54.462v77.361H22.307z"
                            />
                            <path fill="#5D6F78" d="M232.649,482.78V308.955h32.155V482.78H232.649z"/>
                            <path fill="#5D6F78" d="M332.407,482.78V308.955h34.172c6.17,9.651,18.707,29.11,37.613,58.377
                            c18.904,29.269,30.414,47.065,34.527,53.394c1.187,1.82,3.223,4.885,6.111,9.195c2.887,4.313,4.924,7.377,6.111,9.195
                            c-0.555-27.131-0.831-41.211-0.831-42.24v-87.921h29.308V482.78h-34.646l-78.43-122.211l-5.695-9.374
                            c0.632,20.883,0.949,35.557,0.949,44.021v87.564H332.407z"/>
                            <path fill="#5D6F78" d="M611.805,416.615v66.165h-32.273V334.702h-50.665v-25.748h133.365v25.748h-50.427V416.615"/>
                            <path fill="#5D6F78" d="M762.347,408.208c-4.746-1.383-9.195-2.729-13.349-4.035c-4.153-1.305-7.139-2.313-8.958-3.025
                            c-23.335-8.226-35.002-23.571-35.002-46.037c0-15.74,5.595-27.883,16.789-36.426c11.192-8.543,26.044-12.814,44.554-12.814
                            c20.961,0,39.036,4.312,54.224,12.933l-9.729,23.493c-15.504-6.96-29.9-10.441-43.189-10.441c-9.571,0-16.789,1.681-21.654,5.042
                            c-4.865,3.363-7.457,8.958-7.771,16.79c0,9.651,4.785,15.94,14.357,18.866c1.898,0.712,6.564,2.254,14.001,4.627
                            c7.435,2.373,12.26,3.916,14.476,4.627c15.899,5.221,27.27,11.707,34.112,19.459c6.841,7.753,10.264,18.551,10.264,32.393
                            c0,16.533-5.854,29.247-17.561,38.146c-11.708,8.898-27.488,13.348-47.343,13.348c-20.962,0-39.748-4.824-56.359-14.475
                            l7.831-25.511c7.434,4.114,15.523,7.437,24.264,9.967c8.74,2.532,17.264,3.797,25.57,3.797c10.204,0,17.916-2.195,23.137-6.585
                            c5.221-4.391,7.831-10.697,7.831-18.926c0-10.916-7.278-18.469-21.832-22.662C769.979,410.443,767.093,409.592,762.347,408.208"/>
                            </g>
                            </svg>
                          </a>
                      </h1>
                </div>
                <div class="col-sm-9 col-xs-4 clearfix">
                    <div class="ch-l-nav__new">
                        <div class="ch-l-nav--holder__new">
                              <!-- <span class="ch-l-nav--span"></span>
                              <span class="ch-l-nav--span"></span>
                              <span class="ch-l-nav--span"></span> -->
                              <div class="ch-l-nav--holder__svg">
                                <svg viewBox="0 0 800 600">
                                  <path d="M300,220 C300,220 520,220 540,220 C740,220 640,540 520,420 C440,340 300,200 300,200" id="top"></path>
                                  <path d="M300,320 L540,320" id="middle"></path>
                                  <path d="M300,210 C300,210 520,210 540,210 C740,210 640,530 520,410 C440,330 300,190 300,190" id="bottom" transform="translate(480, 320) scale(1, -1) translate(-480, -318) "></path>
                                </svg>
                              </div>
                        </div>
                        <div class="ch-l-nav__new-outer">
                            <div class="ch-l-nav__table">
                                <div class="ch-l-nav__cell">
                                    <div class="ch-l-nav__scroll">
                                        <?php wp_nav_menu(); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      
      <!-- <div class="ch-l-nav__overlay"></div> -->
      <!-- <div class="ch-l-nav--wrap">
          
          
          
      </div> -->
    </header>

    <div class="ch-l-theme__switcher">
        <h4>STYLE SWITCHER</h4>
        <div class="ch-l-theme__wrap">
            <div class="ch-l-theme__block active" data-theme="default"></div>
            <div class="ch-l-theme__block ch-l-theme__dark" data-theme="ch-l-theme__dark"></div>
            <!-- <div class="ch-l-theme__block ch-l-theme__1" data-theme="ch-l-theme_1"></div>
            <div class="ch-l-theme__block ch-l-theme__2" data-theme="ch-l-theme_2"></div>    
            <div class="ch-l-theme__block ch-l-theme__3" data-theme="ch-l-theme_3"></div>
            <div class="ch-l-theme__block ch-l-theme__4" data-theme="ch-l-theme_4"></div>
            <div class="ch-l-theme__block ch-l-theme__5" data-theme="ch-l-theme_5"></div>
            <div class="ch-l-theme__block ch-l-theme__6" data-theme="ch-l-theme_6"></div>
            <div class="ch-l-theme__block ch-l-theme__7" data-theme="ch-l-theme_7"></div> -->
        </div>
        <div class="ch-l-theme__icon">
            <i class="fa fa-cog" aria-hidden="true"></i>
        </div>
    </div>
        
        