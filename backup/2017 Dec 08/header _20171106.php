<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
<meta charset="<?php bloginfo('charset'); ?>">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="google-site-verification" content="8UeiofC42IIbyMF26po9sm_teSV9oqaUuuZbLsW21z4" />
<title><?php bloginfo('name'); { echo ' | '; } if(wp_title('', false)) {} else { echo bloginfo('description'); }wp_title(''); ?></title>

<meta name="description" content="Codehints, Anish M Alias, HTML, HTML5, CSS, CSS3, Jquery, Wordpress, Android, SASS, LESS, Angular">
<meta name="viewport" content="width=device-width, initial-scale=1">

<?php wp_head(); ?>

<link rel="shortcut icon" href="<?php echo get_bloginfo('template_directory'); ?>/img/fav-icon.png" />
<link rel="stylesheet" href="<?php echo get_bloginfo('template_directory'); ?>/css/main.css">
<link href="https://fonts.googleapis.com/css?family=Poppins:100,300,400,500,600,700" rel="stylesheet">
<script src="<?php echo get_bloginfo('template_directory'); ?>/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-67055077-1', 'auto');
  ga('send', 'pageview');

</script>

</head>
	<body>
		<header class="header">
        <?php get_template_part( 'dummy', get_post_format() ); ?>
      <h1 class="ch-l-logo">
          <a href="<?php echo home_url();?>">
              <i class="codehints-logo"></i>
              <div class="ch-l-logo--text">
                  <span class="ch-l-logo--large">Codehints</span>
                  <span class="ch-l-logo--small">Stop your search here</span>
              </div>
          </a>
      </h1>
      <!-- <div class="ch-l-nav__overlay"></div> -->
      <div class="ch-l-nav--wrap">
          <div class="ch-l-nav--holder">
              <span class="ch-l-nav--span"></span>
              <span class="ch-l-nav--span"></span>
              <span class="ch-l-nav--span"></span>
          </div>
          
          <?php
             wp_nav_menu( array( 
              'menu' => 'anish2',
              'container' => 'menu'
               ) );   
          ?>
      </div>
    </header>
        
        