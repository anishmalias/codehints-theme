<?php
/**
 * The Template for displaying all single posts.
 */

get_header(); ?>
<?php get_template_part( 'spotlight-inner', get_post_format() ); ?>
<section class="ch-l-content-section">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-sm-8">
                <div class="ch-l-post--list">
                    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                    <div class="ch-l-post--block">
                        <div <?php post_class() ?> id="post-<?php the_ID(); ?>">
        					
        					<div class="ch-l-post--content ch-l-post--content__inside">
        						<h2><?php the_title(); ?></h2>
        						<div class="ch-l-post__info">
                                    <div class="ch-l-post__user">
                                        <div class="ch-l-post__image">
                                            <?php echo get_avatar( get_the_author_meta('user_email'), $size = '50'); ?>
                                        </div>
                                        <div class="ch-l-post__user-content">
                                            <h5><?php the_author_posts_link() ?></h5>
                                            <span class="ch-l-post__date">
                                                <?php the_time('M j\<\s\u\p\>S\<\/\s\u\p\>, Y') ?>
                                            </span>
                                        </div>
                                    </div>
                                    
                                    <div class="ch-l-post__comment-count">
                                        <span>
                                            <?php
                                                if($post->comment_count > 0) { 

                                                echo '';

                                                comments_popup_link('', '1 ', '% '); 

                                                }else if($post->comment_count == 0) {
                                                    echo '0';
                                                } 
                                            ?>
                                        </span>
                                    </div>

                                    <div class="ch-l-post__tags"> 
                                        <?php the_category(' , '); ?>
                                    </div>
                                </div>
        				
        						<div class="entry">
        							<?php the_content(); ?>
        						</div>
        					</div>
                    
                            <!-- <p class="postmetadata"><?php the_tags('Tags: ', ', ', '<br />'); ?> Posted in <?php the_category(', ') ?> | <?php edit_post_link('Edit', '', ' | '); ?>  <?php comments_popup_link('No Comments »', '1 Comment »', '% Comments »'); ?></p> -->
                        </div>
                    </div>
                    <?php endwhile; endif; ?>
                </div>
                <div class="ch-l-post__recent">
                    <h4>Recent Article</h4>
                    <div class="ch-l-post__recent-outer">
                        <div class="ch-l-post__loader">
                            <div class="windows8">
                                <div class="wBall" id="wBall_1">
                                    <div class="wInnerBall"></div>
                                </div>
                                <div class="wBall" id="wBall_2">
                                    <div class="wInnerBall"></div>
                                </div>
                                <div class="wBall" id="wBall_3">
                                    <div class="wInnerBall"></div>
                                </div>
                                <div class="wBall" id="wBall_4">
                                    <div class="wInnerBall"></div>
                                </div>
                                <div class="wBall" id="wBall_5">
                                    <div class="wInnerBall"></div>
                                </div>
                            </div>
                        </div>
                        <ul>
                            <?php
                                $new_loop = new WP_Query( array(
                                'post_type' => 'Post',
                                'orderby'   => 'rand',
                                'posts_per_page' => 15
                                ) );
                            ?>

                            <?php if ( $new_loop->have_posts() ) : ?>
                            <?php while ( $new_loop->have_posts() ) : $new_loop->the_post(); ?>
                                <li>
                                    
                                    <div class="ch-l-thumbs__recents">
                                        <a href="<?php the_permalink(); ?>">
                                            <?php if ( has_post_thumbnail($post->ID) ) {

                                                the_post_thumbnail();

                                            } else { ?>

                                                <img src="<?php bloginfo('template_directory'); ?>/img/default-image.png" alt="<?php the_title(); ?>" />

                                            <?php } ?> 
                                        </a>
                                    </div>
                                    <h5><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
                                    </li>
                            <?php endwhile;?>
                            <?php else: ?>
                            <?php endif; ?>
                            <?php wp_reset_query(); ?>
                        </ul>
                    </div>
                </div>
                <div class="ch-l-comment--wrap">
                    <?php comments_template(); ?>
                </div>
            </div>
            <?php get_sidebar(); ?>
        </div>
    </div>
</section>

<?php get_footer(); ?>

