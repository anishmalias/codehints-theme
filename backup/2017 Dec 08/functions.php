<?php
	
	add_action( 'wp_enqueue_scripts', 'enqueue_child_theme_styles', PHP_INT_MAX);
	function enqueue_child_theme_styles() {
		wp_enqueue_style( 'parent-style', get_template_directory_uri().'/style.css' );
	}

    add_filter( 'post_thumbnail_html', 'remove_thumbnail_dimensions', 10, 3 ); function remove_thumbnail_dimensions( $html, $post_id, $post_image_id ) { $html = preg_replace( '/(width|height)=\"\d*\"\s/', "", $html ); return $html; }


    // featured images

    add_theme_support( 'post-thumbnails' );
    
    	// pagination


    function wpbeginner_numeric_posts_nav() {

	if( is_singular() )
		return;

	global $wp_query;

	/** Stop execution if there's only 1 page */
	if( $wp_query->max_num_pages <= 1 )
		return;

	$paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
	$max   = intval( $wp_query->max_num_pages );

	/**	Add current page to the array */
	if ( $paged >= 1 )
		$links[] = $paged;

	/**	Add the pages around the current page to the array */
	if ( $paged >= 3 ) {
		$links[] = $paged - 1;
		$links[] = $paged - 2;
	}

	if ( ( $paged + 2 ) <= $max ) {
		$links[] = $paged + 2;
		$links[] = $paged + 1;
	}

	echo '<div class="navigation"><ul>' . "\n";

	/**	Previous Post Link */
	if ( get_previous_posts_link() )
		printf( '<li class="prev">%s</li>' . "\n", get_previous_posts_link() );

	/**	Link to first page, plus ellipses if necessary */
	if ( ! in_array( 1, $links ) ) {
		$class = 1 == $paged ? ' class="active"' : '';

		printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );

		if ( ! in_array( 2, $links ) )
			echo '<li>…</li>';
	}

	/**	Link to current page, plus 2 pages in either direction if necessary */
	sort( $links );
	foreach ( (array) $links as $link ) {
		$class = $paged == $link ? ' class="active"' : '';
		printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
	}

	/**	Link to last page, plus ellipses if necessary */
	if ( ! in_array( $max, $links ) ) {
		if ( ! in_array( $max - 1, $links ) )
			echo '<li>…</li>' . "\n";

		$class = $paged == $max ? ' class="active"' : '';
		printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
	}

	/**	Next Post Link */
	if ( get_next_posts_link() )
		printf( '<li class="next">%s</li>' . "\n", get_next_posts_link() );

	echo '</ul></div>' . "\n";

}

	// Include better comments file
	require_once( get_template_directory() .'/functions/better-comments.php' );
	// Include better comments file
	function wpb_move_comment_field_to_bottom( $fields ) {
		$comment_field = $fields['comment'];
		unset( $fields['comment'] );
		$fields['comment'] = $comment_field;
		return $fields;
	}

	/*Add Google captcha field to Comment form*/

	add_filter('comment_form','add_google_captcha');

	function add_google_captcha(){
	    echo '<div class="g-recaptcha" data-sitekey= "6LdGJDoUAAAAAFkxGmkr08ub3hrCWkm4Ue2NrOki
"></div>';
	}

	add_filter( 'comment_form_fields', 'wpb_move_comment_field_to_bottom' );

	// enable menu

	add_theme_support( 'menus' );   

	// widgets
		if ( function_exists('register_sidebar') )
			register_sidebar(array(
	    			'name' => 'sidebar',
	    			'id' => 'sidebar'
	    		)
		    );


		  register_sidebar(array(
		    'name' => 'header_area',
		    'id' => 'header-widget',
		    'before_widget' => '<div class = "widgetizedArea">',
		    'after_widget' => '</div>',
		    'before_title' => '<h3>',
		    'after_title' => '</h3>'
		  )
		);

	// search

function search_form_no_filters() {
  // look for local searchform template
  $search_form_template = locate_template( 'searchform.php' );
  if ( '' !== $search_form_template ) {
    // searchform.php exists, remove all filters
    remove_all_filters('get_search_form');
  }
}
add_action('pre_get_search_form', 'search_form_no_filters');

add_image_size( 'album-grid', 225, 150, true );

?>