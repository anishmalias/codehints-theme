
<section class="ch-l-content-section">

    <div class="container">

        <div class="row">

            <div class="col-md-9 col-sm-8">





                <div class="ch-l-post--list">

                    <?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

                        $args = array( 'post_type' => 'post', 'posts_per_page' => 5, 'paged' => $paged );

                        $wp_query = new WP_Query($args);

                        while ( have_posts() ) : the_post(); ?>

                        

                        <div class="ch-l-post--block">

                            <div class="ch-l-post--image">

                                <a href="<?php the_permalink(); ?>">

                                <?php if ( has_post_thumbnail($post->ID) ) {

                                    the_post_thumbnail();

                                } else { ?>

                                    <img src="<?php bloginfo('template_directory'); ?>/img/default-image.png" alt="<?php the_title(); ?>" />

                                <?php } ?> 

                                </a>

                            </div>

                            <div class="ch-l-post--content">

                                <div class="ch-l-post__info">
                                    <div class="ch-l-post__user">
                                        <div class="ch-l-post__image">
                                            <?php echo get_avatar( get_the_author_meta('user_email'), $size = '50'); ?>
                                        </div>
                                        <div class="ch-l-post__user-content">
                                            <h5><?php the_author_posts_link() ?></h5>
                                            <span class="ch-l-post__date">
                                                <?php the_time('M j\<\s\u\p\>S\<\/\s\u\p\>, Y') ?>
                                            </span>
                                        </div>
                                    </div>
                                    
                                    <div class="ch-l-post__comment-count">
                                        <span>
                                            <?php
                                                if($post->comment_count > 0) { 

                                                echo '';

                                                comments_popup_link('', '1 ', '% '); 

                                                }else if($post->comment_count == 0) {
                                                    echo '0';
                                                } 
                                            ?>
                                        </span>
                                    </div>

                                    <div class="ch-l-post__tags"> 
                                        <?php the_category(' , '); ?>
                                    </div>
                                </div>
                                <h2><a href="<?php the_permalink(); ?>"><?php the_title() ?></a></h2>
                                <div class="ch-l-post-content--wrap">
                                    <?php the_excerpt(); ?>
                                    <a href="<?php the_permalink(); ?>" class="ch-c-btn--more">More</a>
                                </div>

                            </div>

                        </div>

                    <?php endwhile; ?>





                </div>



                <div class="ch-l-pagination">

                    <!-- then the pagination links -->

                    <?php wpbeginner_numeric_posts_nav(); ?>



                </div>

            </div>

            <?php get_sidebar(); ?>

        </div>

    </div>

</section>

       