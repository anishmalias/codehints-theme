<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
<meta charset="<?php bloginfo('charset'); ?>">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="google-site-verification" content="8UeiofC42IIbyMF26po9sm_teSV9oqaUuuZbLsW21z4" />
<title><?php bloginfo('name'); { echo ' | '; } if(wp_title('', false)) {} else { echo bloginfo('description'); }wp_title(''); ?></title>

<meta name="description" content="Codehints, Anish M Alias, HTML, HTML5, CSS, CSS3, Jquery, Wordpress, Android, SASS, LESS, Angular">
<meta name="viewport" content="width=device-width, initial-scale=1">

<?php wp_head(); ?>

<link rel="shortcut icon" href="<?php echo get_bloginfo('template_directory'); ?>/img/fav-icon.png" />
<link rel="stylesheet" href="<?php echo get_bloginfo('template_directory'); ?>/css/main.css">
<link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700,900" rel="stylesheet">
<script src="<?php echo get_bloginfo('template_directory'); ?>/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-67055077-1', 'auto');
  ga('send', 'pageview');

</script>



</head>
	<body>
		<header class="header ch-l-header__new">
        <?php get_template_part( 'dummy', get_post_format() ); ?>
        <div class="container">
            <div class="row">
                <div class="col-sm-3 col-xs-8">
                    <h1 class="ch-l-logo__new">
                          <a href="<?php echo home_url();?>">
                              <img src="<?php bloginfo('template_directory'); ?>/img/img-logo.png" class="img-responsive">
                          </a>
                      </h1>
                </div>
                <div class="col-sm-9 col-xs-4 clearfix">
                    <div class="ch-l-nav__new">
                        <div class="ch-l-nav--holder__new">
                              <!-- <span class="ch-l-nav--span"></span>
                              <span class="ch-l-nav--span"></span>
                              <span class="ch-l-nav--span"></span> -->
                              <div class="ch-l-nav--holder__svg">
                                <svg viewBox="0 0 800 600">
                                  <path d="M300,220 C300,220 520,220 540,220 C740,220 640,540 520,420 C440,340 300,200 300,200" id="top"></path>
                                  <path d="M300,320 L540,320" id="middle"></path>
                                  <path d="M300,210 C300,210 520,210 540,210 C740,210 640,530 520,410 C440,330 300,190 300,190" id="bottom" transform="translate(480, 320) scale(1, -1) translate(-480, -318) "></path>
                                </svg>
                              </div>
                        </div>
                        <div class="ch-l-nav__new-outer">
                            <div class="ch-l-nav__table">
                                <div class="ch-l-nav__cell">
                                    <div class="ch-l-nav__scroll">
                                        <?php
                                             wp_nav_menu( array( 
                                              'menu' => 'anish2',
                                              'container' => 'menu'
                                               ) );   
                                          ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      
      <!-- <div class="ch-l-nav__overlay"></div> -->
      <!-- <div class="ch-l-nav--wrap">
          
          
          
      </div> -->
    </header>

    <div class="ch-l-theme__switcher">
        <h4>STYLE SWITCHER</h4>
        <div class="ch-l-theme__wrap">
            <div class="ch-l-theme__block active" data-theme="default"></div>
            <div class="ch-l-theme__block ch-l-theme__1" data-theme="ch-l-theme_1"></div>
            <div class="ch-l-theme__block ch-l-theme__2" data-theme="ch-l-theme_2"></div>    
            <div class="ch-l-theme__block ch-l-theme__3" data-theme="ch-l-theme_3"></div>
            <div class="ch-l-theme__block ch-l-theme__4" data-theme="ch-l-theme_4"></div>
            <div class="ch-l-theme__block ch-l-theme__5" data-theme="ch-l-theme_5"></div>
            <div class="ch-l-theme__block ch-l-theme__6" data-theme="ch-l-theme_6"></div>
            <div class="ch-l-theme__block ch-l-theme__7" data-theme="ch-l-theme_7"></div>
        </div>
        <div class="ch-l-theme__icon">
            <i class="fa fa-cog" aria-hidden="true"></i>
        </div>
    </div>
        
        