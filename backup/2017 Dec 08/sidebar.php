<div class="col-md-3 col-sm-4 ch-l-siderbar--wrapper">

	<div class="ch-l-sidebar">

		<div class="ch-l-sidebar--widject">

			<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("sidebar") ) : ?>

                 <?php endif;?>
		</div>

		 <div class="cs-l-sidebar--category">
			<h4>Category</h4>
			<ul>

				<?php

					$categories = get_categories();

					foreach($categories as $category) {

					echo '<li><a href="' . get_category_link($category->term_id) . '">' . $category->name . '</a></li>'; 

					}

				?>

			</ul>
		</div> 
		<div >
			<ul>

<?php $the_query = new WP_Query( 'posts_per_page=5' ); ?>


<?php while ($the_query -> have_posts()) : $the_query -> the_post(); ?>


<li><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></li>


<li><?php the_excerpt(__('(more…)')); ?></li>

<?php 
endwhile;
wp_reset_postdata();
?>
</ul>
		</div>
	</div>
</div>







<!-- <div class="ch-l-sidebar cs-l-sidebarr--archives">

	<h4>Archives</h4>

	<ul class="list-unstyled">

		<?php wp_get_archives( 'type=monthly' ); ?>

	</ul>

</div>

 -->