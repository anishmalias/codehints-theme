<footer class="footer">
	<div class="container">
		<div class="ch-l-footer__widget">
            <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("footer") ) : ?>

            <?php endif;?>

            
		</div>
		<div class="ch-l-footer__social">
			<ul>
				<!-- <li>
					<a href="https://www.youtube.com/user/Anish9809535955" target="_blank" class="ch-l-social--youtube"><i class="fa fa-youtube-play" aria-hidden="true"></i></a>
				</li> -->
				<li>
					<a href="https://www.linkedin.com/profile/view?id=222056809&trk=nav_responsive_tab_profile" target="_blank" class="ch-l-social--linkedin"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
				</li>
				<li>
					<a href="https://plus.google.com/u/0/b/105212958141174918577/105212958141174918577/posts" target="_blank" class="ch-l-social--gplus"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
				</li>
				<li>
					<a href="https://www.facebook.com/codehints?ref=hl" class="ch-l-social--fb" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
				</li>
			</ul>
		</div>
		<!-- <div class="row">
			<div class="col-sm-6">
				<div class="ch-l-copyright">
					<span>CodeHints</span> &copy; 2017. All Rights Reserved.
				</div>
			</div>
			<div class="col-sm-6">
				<div class="ch-l-social clearfix">
					<ul>
						<li>
							<a href="https://www.youtube.com/user/Anish9809535955" target="_blank" class="ch-l-social--youtube"><i class="fa fa-youtube-play" aria-hidden="true"></i></a>
						</li>
						<li>
							<a href="https://www.linkedin.com/profile/view?id=222056809&trk=nav_responsive_tab_profile" target="_blank" class="ch-l-social--linkedin"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
						</li>
						<li>
							<a href="https://plus.google.com/u/0/b/105212958141174918577/105212958141174918577/posts" target="_blank" class="ch-l-social--gplus"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
						</li>
						<li>
							<a href="https://www.facebook.com/codehints?ref=hl" class="ch-l-social--fb" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
						</li>
					</ul>
				</div>
			</div>
		</div> -->
	</div>
</footer>

<a href="#" class="page-bottom-scroll"></a>

<?php wp_footer(); ?> 
<!-- Include scripts -->

<script src="<?php echo get_bloginfo('template_directory'); ?>/js/vendor/jquery-1.11.2.min.js" type="text/javascript"></script>
<script src="<?php echo get_bloginfo('template_directory'); ?>/js/vendor/bootstrap.js" type="text/javascript"></script>
<script src="<?php echo get_bloginfo('template_directory'); ?>/js/typeit.min.js" type="text/javascript"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>
<script src="<?php echo get_bloginfo('template_directory'); ?>/js/main.js" type="text/javascript"></script>

<script type="text/javascript">
jQuery("#submit").click(function(e){
var data_2;
jQuery.ajax({
        type: "POST",
        url: "http://codehints.in/wp-content/themes/codehints/google_captcha.php",
        data: jQuery('#commentform').serialize(),
        async:false,
        success: function(data) {
         if(data.nocaptcha==="true") {
       data_2=1;
          } else if(data.spam==="true") {
       data_2=1;
          } else {
       data_2=0;
          }
        }
    });
    if(data_2!=0) {
      e.preventDefault();
      if(data_2==1) {
        alert("Please check the captcha");
      } else {
        alert("Please Don't spam");
      }
    } else {
        jQuery("#commentform").submit
   }
  });
</script>
<script type="text/javascript">
	$(".ch-l-theme__block").click(function(){
		$(".ch-l-theme__block").removeClass("active");
		$(this).addClass("active");
		var themeName = $(this).attr('data-theme');
		$("body").removeClass (function (index, className) {
		    return (className.match (/\bch-l-theme\S+/g) || []).join(' ');
		});
		$("body").addClass(themeName);

	     $.cookie("themeSelect",themeName);
	     var dataValue = $.cookie("themeSelect");
	});
	
	$(window).load(function(){
	if ($.cookie('themeSelect')) {
		var dataValue = $.cookie("themeSelect");
		$("body").removeClass (function (index, className) {
		    return (className.match (/\bch-l-theme\S+/g) || []).join(' ');
		});
        $("body").addClass(dataValue);
        $(".ch-l-theme__block").removeClass("active");
        $(".ch-l-theme__block").each(function(){
        	var themeSelect = $(this).attr('data-theme');
        	if( themeSelect == dataValue){
        		$(this).addClass("active");
        	}
        });
    }
});

</script>

<script type="text/javascript">
    [].forEach.call(document.querySelectorAll('img[data-src]'),    function(img) {
        img.setAttribute('src', img.getAttribute('data-src'));
        img.onload = function() {
            img.removeAttribute('data-src');
        };
    });
</script>