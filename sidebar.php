<div class="col-md-3 col-sm-4 ch-l-siderbar--wrapper">

	<div class="ch-l-sidebar">

			<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("sidebar") ) : ?>

            <?php endif;?>
         <div class="cs-l-sidebar--popular">
			<h4>Popular</h4>

			<ul>

				<?php
                    $new_loop = new WP_Query( array(
                    'posts_per_page' => 5, 
                    'meta_key' => 'popular_posts', 
                    'orderby' => 'meta_value_num', 
                    'order' => 'ASC' 
                    ) );

               
                ?>

                <?php if ( $new_loop->have_posts() ) : ?>
				<?php while ( $new_loop->have_posts() ) : $new_loop->the_post(); ?>


				<li>
					<div class="ch-l-popular__wrap">
						<div class="ch-l-popular__cell">
							<div class="ch-l-popular__avatar">

								<a href="<?php the_permalink(); ?>">
									
	                                <?php if ( has_post_thumbnail($post->ID) ) {
	                                   
	                                    $thumb_id = get_post_thumbnail_id();
                                    	$thumb_url = wp_get_attachment_image_src($thumb_id,'wpse73058', true);                                                                  
                                    ?>
                                    
                                   	 <img data-src="<?php echo $thumb_url[0]; ?>" alt="<?php the_title(); ?>" />

                               		<?php
	                                } else { ?>
	                                    <img src="<?php bloginfo('template_directory'); ?>/img/img-fallback.jpg" class="ch-l-popular__fallback" alt="<?php the_title(); ?>" />
	                                <?php } ?> 
	                            </a>
	                        </div>
						</div>
						<div class="ch-l-popular__content">
							<h4><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h4>
							<span class="ch-l-popular__date">
                                <?php the_time('M j\<\s\u\p\>S\<\/\s\u\p\>, Y') ?>
                            </span>
						</div>
					</div>
				</li>


				

				<?php endwhile;?>
                <?php else: ?>
                <?php endif; ?>
				<?php wp_reset_postdata(); ?>
				
			</ul>
		</div>

		 <div class="cs-l-sidebar--category">
			<h4>Category</h4>
			<ul>

				<?php

					$categories = get_categories();

					foreach($categories as $category) {

					echo '<li><a href="' . get_category_link($category->term_id) . '">' . $category->name . ' <span>'.$category->count.'</span></a></li>'; 

					}

				?>

			</ul>
		</div> 
		
	</div>
</div>







<!-- <div class="ch-l-sidebar cs-l-sidebarr--archives">

	<h4>Archives</h4>

	<ul class="list-unstyled">

		<?php wp_get_archives( 'type=monthly' ); ?>

	</ul>

</div>

 -->