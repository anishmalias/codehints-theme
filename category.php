

<?php get_header(); ?>



<?php get_template_part( 'spotlight-inner', get_post_format() ); ?>

<?php 

			// Check if there are any posts to display

			if ( have_posts() ) : ?>
<div class="ch-l-category__fixed">
			<h2 class="ch-l-category--title"><?php single_cat_title( '', true ); ?></h2>
</div>
<section  class="ch-l-content-section">

	<div class="container">

		<div class="ch-l-category--wrap">
						<?php

						// Display optional category description

						 if ( category_description() ) : ?>

						<div class="archive-meta"><?php echo category_description(); ?></div>

						<?php endif; ?>

						

							<div class="ch-l-category--base">

	 							<?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

	                                $args = array( 'post_type' => 'post', 'posts_per_page' => 1, 'paged' => $paged );

	                               



							// The Loop

							while ( have_posts() ) : the_post(); ?> 

							<div class="ch-l-category--block">
								<div class="ch-l-post--image">

									<a href="<?php the_permalink(); ?>">

										<?php if ( has_post_thumbnail($post->ID) ) {

                                            $thumb_id = get_post_thumbnail_id();
		                                    $thumb_url = wp_get_attachment_image_src($thumb_id,'thumbnail-size', true);
		                                    
		                                    
		                                    ?>
                                    
                                    			<img data-src="<?php echo $thumb_url[0]; ?>" alt="<?php the_title(); ?>" />

                               				<?php

                                        } else { ?>

                                            <img src="<?php bloginfo('template_directory'); ?>/img/default-image.png" alt="<?php the_title(); ?>" />

                                        <?php } ?> 

                                    </a>

								</div>	
								

								<div class="ch-l-post__info">
                                    <div class="ch-l-post__user">
                                        <div class="ch-l-post__image">
                                            <?php echo get_avatar( get_the_author_meta('user_email'), $size = '50'); ?>
                                        </div>
                                        <div class="ch-l-post__user-content">
                                            <h5><?php the_author_posts_link() ?></h5>
                                            <span class="ch-l-post__date">
                                                <?php the_time('M j\<\s\u\p\>S\<\/\s\u\p\>, Y') ?>
                                            </span>
                                        </div>
                                    </div>
                                    
                                    <div class="ch-l-post__comment-count">
                                        <span>
                                            <?php
                                                if($post->comment_count > 0) { 

                                                echo '';

                                                comments_popup_link('', '1 ', '% '); 

                                                }else if($post->comment_count == 0) {
                                                    echo '0';
                                                } 
                                            ?>
                                        </span>
                                    </div>

                                    <div class="ch-l-post__tags"> 
                                        <?php the_category(' , '); ?>
                                    </div>
                                </div>

                                <h3><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
								<div class="entry">

									<div class="ch-l-content__trim">
                                        <?php echo get_the_content() ?>
                                    </div>     

									 <!-- <p class="postmetadata"><?php

									  comments_popup_link( 'No comments yet', '1 comment', '% comments', 'comments-link', 'Comments closed');

									?></p> -->



									<a href="<?php the_permalink(); ?>" class="ch-c-btn--more">More</a>

								</div>

							</div>

					<?php endwhile; 



				else: ?>

				</div></div>

				<p>Sorry, no posts matched your criteria.</p>

				<?php endif; ?>



				</div>

			<div class="ch-l-pagination" style="margin-top: 0;">

                    <!-- then the pagination links -->

                    <?php wpbeginner_numeric_posts_nav(); ?>



                </div>



</section>



<?php get_footer(); ?>

