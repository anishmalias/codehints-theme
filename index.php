
<?php get_header(); ?>

<?php get_template_part( 'spotlight', get_post_format() ); ?>

<?php get_template_part( 'content', get_post_format() ); ?>

<?php get_footer(); ?>