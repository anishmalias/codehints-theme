<?php /* Template Name: Not Found */ ?>

<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
<meta charset="<?php bloginfo('charset'); ?>">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="google-site-verification" content="8UeiofC42IIbyMF26po9sm_teSV9oqaUuuZbLsW21z4" />
<title><?php bloginfo('name'); { echo ' | '; } if(wp_title('', false)) {} else { echo bloginfo('description'); }wp_title(''); ?></title>

<meta name="description" content="Codehints, Anish M Alias, HTML, HTML5, CSS, CSS3, Jquery, Wordpress, Android, SASS, LESS, Angular">
<meta name="viewport" content="width=device-width, initial-scale=1">

<?php wp_head(); ?>

<link rel="shortcut icon" href="<?php echo get_bloginfo('template_directory'); ?>/img/fav-icon.png" />
<link rel="stylesheet" href="<?php echo get_bloginfo('template_directory'); ?>/css/main.css">
<link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700,900" rel="stylesheet">
<script src="<?php echo get_bloginfo('template_directory'); ?>/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-67055077-1', 'auto');
  ga('send', 'pageview');

</script>
<style type="text/css">
	html{
		margin: 0 !important;
	}
</style>
</head>
	<body>
		<div class="ch-l-not-found">
			<div class="container">
				<div class="ch-l-not-found-row">
					<div class="ch-l-not-found-col ch-l-no-found__content">
						<h1>404</h1>
						<h2>Oops!</h2>
						<p>We can't seem to find the page you're looking for.</p>
						<a href="/" class="ch-c-btn--more">Go to Home</a>
					</div>
					<div class="ch-l-not-found-col ch-l-not-found__image">
						<img src="<?php echo get_bloginfo('template_directory'); ?>/img/404.gif" class="img-responsive">
					</div>
				</div>
			</div>
		</div>
	</body>
</html>






