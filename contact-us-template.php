<?php /* Template Name: Without Category */ ?>



<?php get_header(); ?>

<?php get_template_part( 'spotlight-inner', get_post_format() ); ?>

<?php

		// Start the loop.

		while ( have_posts() ) : the_post(); ?>

<h2 class="ch-l-category--title"><?php  the_title(); ?></h2>

<section class="ch-l-content-section">

	<div class="container">
		<div class="row">
			<div class="col-md-9 col-sm-8">
				<?php  the_content(); ?>
			</div>
			<?php get_template_part( 'sidebar-with-out-category', get_post_format() ); ?>
		</div>
	</div>

</section>



<?php endwhile; // End the loop. ?>

<?php get_footer(); ?>



